package ar.com.rockcodes.residencesigns;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.bekvon.bukkit.residence.Residence;
import com.bekvon.bukkit.residence.event.ResidenceOwnerChangeEvent;
import com.bekvon.bukkit.residence.event.ResidenceRentEvent;
import com.bekvon.bukkit.residence.protection.ClaimedResidence;

public class ResSigns extends JavaPlugin implements Listener {

	
	public Map<String,Location> signsmap;
	
	@Override
	public void onDisable() {
		try {
			this.saveSigns();
		} catch (IOException e) {
			ResSigns.this.getLogger().info("Error al guardar resings "+e.getMessage());
		}
	}

	@Override
	public void onEnable() {

		PluginManager pluginmanager = this.getServer().getPluginManager();
		
		if(pluginmanager.isPluginEnabled("Residence")){
			this.signsmap = new HashMap<String,Location>();
			this.loadSigns();
			this.getServer().getPluginManager().registerEvents(this, this);
			
		}
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				ResSigns.this.getLogger().info("Guardando Residence Signs");
				try {
					ResSigns.this.saveSigns();
					ResSigns.this.getLogger().info("ResSigns guardados con exito");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					ResSigns.this.getLogger().info("Error al guardar resings "+e.getMessage());
				}
			}
			
		}, 108000L, 108000L);
		
		
	}
	
	public void loadSigns(){
		
		FileConfiguration signsconf = this.getConfig();
		
		for(String s : signsconf.getKeys(false)){
			if(s.contains("-"))
				s = s.replace("-", ".");
			Location loc = this.unserializeLoc( signsconf.getString(s));
			if(loc ==null) continue;
			this.signsmap.put(s,loc);
		}
		
	}
	
	public void saveSigns() throws IOException{
		
		YamlConfiguration signsconf = new YamlConfiguration();
		
		for(Entry<String,Location> shop: this.signsmap.entrySet()){
			
			String name = shop.getKey();
			
			if(name.contains("."))
				name = name.replace(".", "-");
			
			Location loc = shop.getValue();
			
			if(loc ==null) continue;
			signsconf.set(name,this.serializeLoc( loc ));
			
		}
			
		

		signsconf.save(new File(this.getDataFolder(), "config.yml"));

		
	}

	
	public String serializeLoc(Location loc){
		
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		World w = loc.getWorld();
		String world = w.getName();
		
		return x+","+y+","+z+","+world;
	}
	
	
	
	public Location unserializeLoc(String str){
		if(str==null || str.equals("")){
			//this.degug("Error deserializando location string nula");
			return null;
		}
		
		String[] parts = str.split(",");
		if(parts.length<4){
			//this.degug("Error deserializando location string no tiene suficientes argumentos");
			return null;
		}
		
		int x = Integer.valueOf(parts[0]);
		int y = Integer.valueOf(parts[1]);
		int z = Integer.valueOf(parts[2]);
		World world = Bukkit.getWorld(parts[3]);
		if(world == null){
			//this.degug("Error deserializando location world nulo");
			return null;
		}
		
		return new Location(world,x,y,z);
	}
	
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onSignUpdate(SignChangeEvent e) {

    	Player player = e.getPlayer();
    	
    	SignChangeEvent sign = e;
    	if(!e.getLine(0).equalsIgnoreCase("[Alquiler]") && !e.getLine(0).equalsIgnoreCase("[Venta]")) return;
		ClaimedResidence residence = this.getResBySign(sign);
		
		if(residence ==null ){player.sendMessage(ChatColor.RED+"Residencia Invalida");
		e.getBlock().breakNaturally(); return;}

		if(!residence.getOwner().equals(player.getName())){
		player.sendMessage(ChatColor.RED+"Solo el dueño de la residencia puede hacer esto!");
		e.getBlock().breakNaturally(); return;}
		
		if(e.getLine(0).equalsIgnoreCase("[Alquiler]")) {
			this.ponerAlquiler(player,sign,residence);
		}else if (e.getLine(0).equalsIgnoreCase("[Venta]")){
			this.ponerVenta(player,sign,residence);
		}
			
    }
    
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onrent(ResidenceRentEvent e) {
    	ClaimedResidence res = e.getResidence();
    	if(this.signsmap.containsKey(res.getName())){
    		Sign sign = (Sign) this.signsmap.get(res.getName()).getBlock().getState();
    		this.setSignStatus(sign, true);
    	}
    }
    
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onownerchange(ResidenceOwnerChangeEvent e) {
    	
    	this.getLogger().info("debug changeowner");
    	ClaimedResidence res = e.getResidence();
    	if(this.signsmap.containsKey(res.getName())){
    		Sign sign = (Sign) this.signsmap.get(res.getName()).getBlock().getState();
    		sign.getBlock().breakNaturally();
    		this.signsmap.remove(res.getName());
    	}

    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onBlockBreak(BlockBreakEvent e) {
    	Player player = e.getPlayer();
    	Block b = e.getBlock();
    	if(!this.signsmap.containsValue(b.getLocation()))return;
    	Sign sign = (Sign)b.getState();
    	ClaimedResidence res = this.getResBySign(sign);
    	
    	if(!res.getOwner().equals(player.getName())){
    		e.setCancelled(true); 
    		player.sendMessage(ChatColor.RED+"Solo el dueño de la res puede romper este cartel.");
    		return;
    	}
    	
		if(Residence.getRentManager().isForRent(res.getName())){
			Residence.getRentManager().removeRentable(res.getName());
		}
		else if(Residence.getTransactionManager().isForSale(res.getName())){
			Residence.getTransactionManager().removeFromSale(res.getName());
		}
			

		this.signsmap.remove(res.getName());
		player.sendMessage(ChatColor.DARK_GREEN+"Se ha eliminado el alquiler/venta en la residencia "+res.getName());
	
    }
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerInteract(PlayerInteractEvent e) {
    	
    	Player player = e.getPlayer();
    	if(!e.getAction().equals(Action.RIGHT_CLICK_BLOCK))return;
     	Block b = e.getClickedBlock();	
     	if(b==null) return;
     	if(!this.signsmap.containsValue(b.getLocation()))return;
    	Sign sign = (Sign)b.getState();
    	ClaimedResidence res = this.getResBySign(sign);
		if(res==null) return;
		
		if(sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE+"[Alquiler]")){
			Residence.getRentManager().rent(player, res.getName(), signifrepeat(sign), false);
		}else if (sign.getLine(0).equalsIgnoreCase(ChatColor.DARK_BLUE+"[Venta]")){
			Residence.getTransactionManager().buyPlot(res.getName(), player, false);
		}
    }
    
    public void setSignStatus(Sign sign , boolean status){
    	if(status ) sign.setLine(3,ChatColor.RED+"Alquilada");
		else sign.setLine(3,ChatColor.GREEN+"Libre");
    }
    
    public String getDato(Sign sign , int dato){
    	String[] datos = sign.getLine(1).split("-");
    	if(dato+1>datos.length) return datos[datos.length-1];
    	return datos[dato];
    }
    
    public boolean signifrepeat(Sign sign){
    	return Boolean.parseBoolean(this.getDato(sign, 2));
    }
    
    public ClaimedResidence getResBySign(SignChangeEvent sign){
    	
		ClaimedResidence residence = Residence.getResidenceManager().getByName(sign.getLine(2));
		if (residence==null) residence = Residence.getResidenceManager().getByLoc(sign.getBlock().getLocation()); 
		if (residence==null) return null;
		return residence;
    }
    
    public ClaimedResidence getResBySign(Sign sign){
    	
		ClaimedResidence residence = Residence.getResidenceManager().getByName(sign.getLine(2));
		if (residence==null) residence = Residence.getResidenceManager().getByLoc(sign.getBlock().getLocation()); 
		if (residence==null) return null;
		return residence;
    }
    
    public void ponerAlquiler(Player player , SignChangeEvent sign, ClaimedResidence residence){
    	
    	sign.setLine(0, ChatColor.DARK_BLUE+"[Alquiler]");
		sign.setLine(2, residence.getName());
		String[] datos = sign.getLine(1).split("-");
		//datos 0 = precio 1= dias 2= renovable
		if(Residence.getRentManager().isForRent(residence.getName())){player.sendMessage(ChatColor.RED+"La residencia ya esta en alquiler.");sign.getBlock().breakNaturally(); return;}
		if(Residence.getRentManager().isRented(residence.getName())){player.sendMessage(ChatColor.RED+"La residencia ya esta en alquiler y esta alquilada.");sign.getBlock().breakNaturally(); return;}
		if(datos.length < 3){player.sendMessage(ChatColor.RED+"La segunda linea del cartel es invalida."); sign.getBlock().breakNaturally(); return;} 
		Residence.getRentManager().setForRent(player,residence.getName(), Integer.parseInt(datos[0]), Integer.parseInt(datos[1]),Boolean.parseBoolean(datos[2]),Residence.isResAdminOn(player));
		//player.sendMessage(ChatColor.GREEN+"Se ha puesto en alquiler la residencia "+residence.getName()+" con un costo de "+datos[0]+" cada "+datos[1]+" dias.");
		this.signsmap.put(residence.getName(), sign.getBlock().getLocation());
		sign.setLine(3, ChatColor.GREEN+"Libre");
		sign.setLine(1, ChatColor.BLUE+sign.getLine(1));

			
    }
    
    public void ponerVenta(Player player , SignChangeEvent sign, ClaimedResidence residence){
    	sign.setLine(0, ChatColor.DARK_BLUE+"[Venta]");
    	sign.setLine(2, residence.getName());
		String precio = sign.getLine(1);
		if(Residence.getTransactionManager().isForSale(residence.getName())){player.sendMessage(ChatColor.RED+"La residencia ya esta en venta.");sign.getBlock().breakNaturally(); return;}
		Residence.getTransactionManager().putForSale(residence.getName(), player, Integer.parseInt(precio), Residence.isResAdminOn(player) );
			//player.sendMessage(ChatColor.GREEN+"Se ha puesto en venta la residencia "+residence.getName()+" con un costo de "+precio+"$ .");
		this.signsmap.put(residence.getName(), sign.getBlock().getLocation());
		sign.setLine(3, ChatColor.GREEN+"Libre");
		sign.setLine(1, ChatColor.BLUE+sign.getLine(1));	
    }
	
	
}
